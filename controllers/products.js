	const Product = require('../models/Product');

	module.exports.addProduct = (info) => {
		let name = info.name;
		let desc = info.description;
		let cost = info.price;
		let imgUrl = info.imgUrl;

		let newProduct = new Product({
			name: name,
			description: desc,
			price: cost,
			imgUrl: imgUrl
		}) 
		return newProduct.save().then((savedProduct, err) => {
				if (savedProduct) {
					return savedProduct;
				} else {
					return false;
				}
		});
	}

	module.exports.getAllProduct = () => {
		return Product.find({}).then(result => {
			return result;
		});

	};

	module.exports.getAllActive = () => {
		return Product.find({isActive: true}).then(result => {
			return result;
		});
	};

	module.exports.getProduct = (id) => {
		return Product.findById(id).then(result => {
			return result;

			});
	};

	module.exports.updateProduct = (product, details) => {
	let	name = details.name;
	let	desc = details.description;
	let	cost = details.price;
	let updatedProduct = {
			name: name,
			description: desc,
			price: cost
		};	
		let id = product.productId;
		return Product.findByIdAndUpdate(id,updatedProduct).then((productUpdated, err) => {
			if (productUpdated) {
				return true;
			} else {
				return 'Failed to update Products';
			}

		});
	 };
	 
	 module.exports.archiveProduct = (product) => {
	 		let id = product.productId;
	 		let updates = {
	 			isActive: false
	 		};
	 		
	 	return Product.findByIdAndUpdate(id, updates).then((archived, err) => {
	 		if (archived) {
	 			return 'Product archived';
	 		} else {
	 			return false;
	 		}
	 	});

	 		
	 }
	 //Product Reactivation
	module.exports.unarchiveProduct = (product) => {
		let id = product.productId
		let updates = {
			isActive: true
		};
		return Product.findByIdAndUpdate(id, updates).then((active, err) => {
			if (active) {
				return active;
			} else {
				return 'Failed to update status'
			}
		});
	};
	module.exports.deleteProduct = (productId) => {
		console.log(productId)
				return Product.findById(productId).then(product => {
					if (product === null) {
						return 'No product found'
					} else {
						return product.remove().then((removedProduct, err) => {
							if (err) {
								return 'Failed to remove product'
							} else {
								return 'Successfully Deleted'
							};
						});
					};
				});
			};


