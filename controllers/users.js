	const User = require('../models/User');
	const bcrypt = require('bcrypt');
	const auth = require('../auth')

 	module.exports.registerUser = (data) => {
 		let fName = data.firstName;
 		let lName = data.lastName;
 		let mName = data.middleName;
 		let address = data.address;
 		let email = data.email;
 		let passW = data.password;

 		let newUser = new User({
 			firstName: fName,
 			lastName: lName,
 			middleName: mName,
 			address: address,
 			email: email,
 			password: bcrypt.hashSync(passW, 10),
 		});
 		return newUser.save().then((user, err) =>{
 			if (user) {
 			  return user;
 			} else {
 			  return false;
 			}
 		});
 	}

	module.exports.checkEmailExists = (reqBody) => {
		return User.find({email: reqBody.email}).then(result => {
			if (result.length > 0) {
				return 'Email Already Exists';
			} else {
				return 'Email is still available';
			};
		});
	};

	module.exports.loginUser = (reqBody) => {
		let uEmail = reqBody.email;
		let uPassW = reqBody.password;
		return User.findOne({email: uEmail}).then(result => {
			let passW = result.password;
			if (result === null) {
				return 'Email Does Not Exists!';
			} else {

				const isMatched = bcrypt.compareSync(uPassW, passW)
				if (isMatched) {

					let dataNiUser = result.toObject();
					// return {accessToken: auth.createAccessToken(dataNiUser), userId: dataNiUser._id};



					return {_id: dataNiUser._id, firstName: dataNiUser.firstName, lastName: dataNiUser.lastName, address: dataNiUser.address, isAdmin: dataNiUser.isAdmin, accessToken: auth.createAccessToken(dataNiUser),};
				} else {
					return 'Password Does Not Match. Check Credentials'
				}
			   
			};
		});
	};

	module.exports.getProfile = (id) => {
		return User.findById(id).then(user => {
			return user;
		});
	};
		module.exports.setAsAdmin = (userId) => {
			let updates = {
				isAdmin: true
			}
			return User.findByIdAndUpdate(userId, updates, {new: true}).then((admin, err) => {
				if (admin) {
					return admin.save().then((updatedUser, saveErr) => {
	 					if (saveErr) {
	 						return false;
	 					} else {
	 						return updatedUser;
	 					}
					})
				} else {
					return 'Updates Failed to implement';
				}
			});
		};
	 	
	 	module.exports.setAsNonAdmin = (userId) => {

         let updates = {
            isAdmin: false
         }
         return User.findByIdAndUpdate(userId, updates, {new: true}).then((user, err) => {
            if (user) {
               return user;
            } else {
               return 'Failed to update'
            }
         });
      };

   module.exports.updatePassword = (reqBody) => {
      let uEmail = reqBody.email;
      let uPass = reqBody.password;
      let nPass = reqBody.newpassword;

      return User.findOne({email: uEmail}).then(result => {
         if (result === null) {
            return `Email does not exist!`;
         } else {
         let passW = result.password;
         let isMatched = bcrypt.compareSync(uPass, passW);
            if (isMatched) {
               let dataNiUser = result.toObject();
               let updates = {
                  password: bcrypt.hashSync(nPass, 10)
                  }
               return User.findByIdAndUpdate(dataNiUser, updates).then((user, err) => {
                  if (user) {
                     return `Successfully updated the password!`;
                  } else {
                     return `Unable to update user information.`;
                  };
               });
            } else {
               return `Passwords does not match. Check credentials.`
            };
         };
      });
   };



	module.exports.getAllUser = () => {
		return User.find({}).then(user => {
			console.log("user", user)
			return user;
		});
	};

	module.exports.getUser = (id) => {
		return User.findById(id).then(user => {
			return user;
		});
	};




