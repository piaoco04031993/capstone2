	const User = require('../models/User');
	const auth = require('../auth');
	const Product = require('../models/Product');
	const Order = require('../models/Order');

	module.exports.createOrder = async (data) => {
		let product = data.productId;
		let qty = data.quantity;
		let id = data.id;
		let cost = data.price;

		let total = await Product.findById(product).then(result => result.price*qty);
		
		let newOrder = new Order({
			productId: product,
			quantity: qty,
			totalAmount: total,
			userId: id
		})
		return newOrder.save().then((save, err) => {
			if (err) {
				return `Something went wrong please try again!`
			} else {
				return save;
			}
		});
	};

	module.exports.getAllOrders = (userId) => {
		return Order.find({userId}).populate([{path: 'userId'}, {path: 'productId'}]).then(result => {
			return result;
		})
	}

	module.exports.getAllOrdersAdmin = () => {
		return Order.find().populate([{path: 'userId'}, {path: 'productId'}]).then(result => {
			return result;
		})
	}