	const mongoose = require('mongoose'); 
	const Schema = mongoose.Schema;

	const orderSchema = new mongoose.Schema({
	
		totalAmount: {
			type: Number,
			required: [true, 'Price is Required!']
		},
		
		purchasedOn: {
			type: Date,
			default: new Date()
		},

		userId: {
			type: Schema.Types.ObjectId,
			ref: 'User',
			required: [true, "User Id is Required!"]
		},
		productId: {
			type: Schema.Types.ObjectId,
			ref: 'Product',
			required: [true, "Product Id is Required!"] 
		},
		quantity : {
			type: Number,
			required: [true, "Quantity Required!"]
		}

	});

   module.exports = mongoose.model("Order", orderSchema);
