const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First Name is Required!']
	},
	lastName: {
		type: String,
		required: [true, 'Last Name is Required!']
	},
	middleName: {
		type: String,
		required: [true, 'Middle Name is Required!']
	},
	address: {
		type: String,
		required: [true, 'Address is Required!']
	}, 
	email: {
		type: String,
		required: [true, 'Email is Required!']
	}, 
	password: {
		type: String,
		required: [true, 'Password is Required!']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [
		{
			products: [
				{

				productId: {
					type: String,
					required: [true, "Product Id is required."]
					
			},
				quantity: {
					type: Number, 
					required: [true, "Products Quantity Required."]
					}

			}
			],
			totalAmount: {
					type: Number,
					required: [true, "Total amount is required."]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});
			
module.exports = mongoose.model('User',userSchema);
	


