	const express = require('express');
	const mongoose = require("mongoose");
	const dotenv = require("dotenv");
	const cors = require("cors");
	const productRoutes = require('./routes/products');
	const userRoutes = require('./routes/users');
	const orderRoutes = require('./routes/orders');

	dotenv.config();
	const port = process.env.PORT;
	const credentials = process.env.MONGO_URL;
	
	const app = express();
	app.use(cors());
	app.use(express.json());

	mongoose.connect(credentials);
	const db = mongoose.connection;
	db.once('open', () => console.log(`Connected to Atlas`))

	app.use('/users',userRoutes)
	app.use('/products', productRoutes)
	app.use('/orders', orderRoutes) 
	
	app.get('/', (req, res) => {
		res.send(`Project Deployed Successfully`)
	}); 
	app.listen(port, () => {
   		console.log(`API is now online on port ${port}`);
	});

