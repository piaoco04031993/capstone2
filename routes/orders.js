	const exp = require('express');
	const controller = require('../controllers/orders');
	const auth = require('../auth');

	const route = exp.Router();

	route.post('/orderId', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let userId = payload.id;
		let isAdmin = payload.isAdmin;
		let product = req.body.productId;
		let qty = req.body.quantity;
		let info = {
			id : userId,
			productId: product,
			quantity: qty
		};
		if (!isAdmin) {
			controller.createOrder(info).then(result => {
				res.send(result);
			})
		} else {
			res.send("Admins are not allowed to order!")
		}
	});

	route.get('/', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let userId = payload.id;
		let isAdmin = payload.isAdmin;
		if (!isAdmin) {
			controller.getAllOrders(userId).then(result => {
				res.send(result);
			})
		} else {
			res.send("No Data Found!")
		}
	});

	route.get('/all-orders', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let userId = payload.id;
		let isAdmin = payload.isAdmin;
		if (isAdmin) {
			controller.getAllOrdersAdmin().then(result => {
				res.send(result);
			})
		} else {
			res.send("No Data Found!")
		}
	});

	module.exports = route;