	const exp = require('express');
	const controller = require('../controllers/products');
	const auth = require('../auth');
	const route = exp.Router()

	route.post('/create', (req, res) => {
		let data = req.body;
		controller.addProduct(data).then(outcome => { res.send(outcome)
		});
	});

	route.get('/', (req, res) => {
		controller.getAllProduct().then(outcome => {
			res.send(outcome)
		})
	});

	route.get('/:id', (req,res) => {
		let id = req.params.id
		controller.getProduct(id).then(result => {
			res.send(result);
		});
	});

	route.put('/:productId', (req, res) => {
		let params = req.params;
		let body = req.body;
		controller.updateProduct(params, body).then(outcome => {
			res.send(outcome);
		});
	});

	route.put('/:productId/archive', (req, res) => {
		let params = req.params;
		controller.archiveProduct(params).then(result => {
			res.send(result);
		});
	});
	//Product reactivation
	route.put('/:productId/reactivation', auth.verify, (req,res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let params = req.params;
			(isAdmin) ? 
				controller.unarchiveProduct(params).then(outcome => {
					res.send(outcome);
			})
			:
				res.send('Unauthorized User');

	});

	route.delete('/:delete/productId', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let id = req.params.delete;
		isAdmin ?
		controller.deleteProduct(id).then(outcome => {
			res.send(outcome);
		})
		: res.send('Unauthorized User')
	});



	module.exports = route;




